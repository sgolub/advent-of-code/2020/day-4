package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

const dataKey = "passports"

type validator interface {
	Validate(val string) bool
}

type numberValidator struct {
	Min int
	Max int
}

func (n numberValidator) String() string {
	return fmt.Sprintf("n[%d-%d]", n.Min, n.Max)
}

func (n numberValidator) Validate(val string) bool {
	i, err := strconv.Atoi(val)
	if err != nil {
		return false
	}
	return n.Min <= i && i <= n.Max
}

type stringValidator struct {
	Regex string
}

func (s stringValidator) String() string {
	return fmt.Sprintf("Regex: %s", s.Regex)
}

func (s stringValidator) Validate(val string) bool {
	match, err := regexp.MatchString(s.Regex, val)
	return err == nil && match
}

type groupValidator struct {
	Any        bool
	Validators []validator
}

func (g groupValidator) String() string {
	return fmt.Sprintf("g[%v %s]", g.Any, g.Validators)
}

func (g groupValidator) Validate(val string) bool {
	for _, valid8 := range g.Validators {
		if g.Any == valid8.Validate(val) {
			return g.Any
		}
	}
	return !g.Any
}

type unitValidator struct {
	Unit      string
	Validator numberValidator
}

func (u unitValidator) Validate(val string) bool {
	if strings.HasSuffix(val, u.Unit) {
		val = strings.TrimSuffix(val, u.Unit)
		return u.Validator.Validate(val)
	}
	return false
}

type choiceValidator struct {
	Choices []string
}

func (c choiceValidator) Validate(val string) bool {
	for _, item := range c.Choices {
		if item == val {
			return true
		}
	}
	return false
}

var (
	requiredFields = map[string]validator{
		"byr": numberValidator{
			Min: 1920,
			Max: 2002,
		},
		"iyr": numberValidator{
			Min: 2010,
			Max: 2020,
		},
		"eyr": numberValidator{
			Min: 2020,
			Max: 2030,
		},
		"hgt": groupValidator{
			Any: false,
			Validators: []validator{
				stringValidator{"^\\d{2,3}(in|cm)$"},
				groupValidator{
					Any: true,
					Validators: []validator{
						unitValidator{
							Unit: "in",
							Validator: numberValidator{
								Min: 59,
								Max: 76,
							},
						},
						unitValidator{
							Unit: "cm",
							Validator: numberValidator{
								Min: 150,
								Max: 193,
							},
						},
					},
				},
			},
		},
		"hcl": stringValidator{"^#[a-f0-9]{6}$"},
		"ecl": choiceValidator{
			Choices: []string{
				"amb",
				"blu",
				"brn",
				"gry",
				"grn",
				"hzl",
				"oth",
			},
		},
		"pid": stringValidator{"^\\d{9}$"},
	}
)

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 4
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) []map[string]string {
	result := []map[string]string{}
	data, ok := days.Control().LoadData(d, useSampleData)[dataKey]
	if !ok {
		return result
	}
	for _, pp := range data.([]interface{}) {
		ppSplits := strings.Split(strings.TrimSpace(pp.(string)), " ")
		m := map[string]string{}
		for _, gr := range ppSplits {
			maps := strings.Split(gr, ":")
			m[maps[0]] = maps[1]
		}
		result = append(result, m)
	}
	return result
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D4P1")
	validCount := 0
	for _, pp := range data {
		valid := true
		for field := range requiredFields {
			if _, ok := pp[field]; !ok {
				valid = false
				break
			}
		}
		if valid {
			validCount++
		}
	}
	return fmt.Sprint(validCount)
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D4P2")
	validCount := len(data)
	for _, pp := range data {
		for field, valid8 := range requiredFields {
			if val, ok := pp[field]; !ok || !valid8.Validate(val) {
				validCount--
				break
			}
		}
	}
	return fmt.Sprint(validCount)
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
